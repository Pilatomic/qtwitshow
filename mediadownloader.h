#ifndef MEDIADOWNLOADER_H
#define MEDIADOWNLOADER_H

#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QString>
#include <QFile>
#include <QJsonObject>

class MediaDownloader : public QObject
{
    Q_OBJECT
public:
    explicit MediaDownloader(const QString tweetPath, const QString saveDir, const QJsonObject mediaJSON, QObject *parent = 0);

    QString getErrorString();
    bool isRunning();

signals:
    void success(QString tweetPath, QString mediaPath);
    void failed(QNetworkReply::NetworkError error);

public slots:

    void start(QNetworkAccessManager* nam);
    void abort();

private slots:
    void onDataReceived();
    void onReplyFinished();

private:
    QString tweetPath_;
    QString filePath_;
    QString mediaURL_;

    QFile* file_;

    QNetworkReply* reply_;

    QString findLargestMediaSize(QJsonObject sizesJSON);
};

#endif // MEDIADOWNLOADER_H
