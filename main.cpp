#include "twitstreamer.h"
#include "showplanner.h"
#include "twitdisplay.h"

#include <QApplication>
#include <QSettings>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSettings settings(QStringLiteral("settings.ini"),QSettings::IniFormat);

    TwitStreamer streamer;
    streamer.setAutoStartStreaming(true);
    streamer.setSettingsFile(&settings);

    ShowPlanner planner;
    planner.loadSettings(&settings);

    TwitDisplay display;
    display.setSettingsFile(&settings);
    display.displaySplashScreen();
    display.show();

    QObject::connect(&streamer,&TwitStreamer::newMediaAvailable,&planner,&ShowPlanner::addMediaToShow);
    QObject::connect(&planner,&ShowPlanner::mediaToDisplay,&display,&TwitDisplay::displayTweet);
    QObject::connect(&streamer,&TwitStreamer::linkStatusUpdated,&display,&TwitDisplay::setLinkStatus);

    streamer.startAuthentication();

    return a.exec();
}
