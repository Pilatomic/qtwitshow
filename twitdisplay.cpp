#include "twitdisplay.h"

#include <QPainter>
#include <QPixmap>
#include <QDebug>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QMenu>
#include <QJsonDocument>
#include <QJsonObject>

#define PIXELCOUNT_FONTSIZE_RATIO 21600

TwitDisplay::TwitDisplay(QWidget *parent) :
    QWidget(parent),textPixelSize(96),mediaIsNew(false),settingsFile(NULL)
{

    buildAndEnableContextMenu();

    //Disable the maximize button to avoid interfering with fullscreen mode
    setWindowFlags(windowFlags()&(~Qt::WindowMaximizeButtonHint));

    setLinkStatus(LinkDown);

    newMediaPixMap.load(QStringLiteral(":/new"));
}

void TwitDisplay::buildAndEnableContextMenu(){
    toggleFullScreenAction = new QAction(tr("Full screen") , this);
    toggleFullScreenAction->setCheckable(true);
    toggleFullScreenAction->setChecked(false);
    connect(toggleFullScreenAction,&QAction::toggled,this,&TwitDisplay::setFullScreen);

    quitAction = new QAction(tr("Quit") , this);
    connect(quitAction,&QAction::triggered,this,&TwitDisplay::close);

    this->addAction(toggleFullScreenAction);
    this->addAction(quitAction);

    this->setContextMenuPolicy(Qt::ActionsContextMenu);
}

void TwitDisplay::displayTweet(QString tweetPath, QString imagePath, bool isNew){
    mediaIsNew = isNew;
    //Meta

    QFile tweetFile(tweetPath,this);
    tweetFile.open(QIODevice::ReadOnly);
    QJsonObject tweetObject = QJsonDocument::fromJson(tweetFile.readAll()).object();
    tweetFile.close();

    QString completeText = tweetObject.value("text").toString();
    QJsonObject user = tweetObject.value("user").toObject();
    QString userName = user.value("screen_name").toString();

    //removing tweet URL
    int urlPos = completeText.lastIndexOf("http");

    if(settingsFile && settingsFile->value("Display/ShowUserName",QVariant(false)).toBool()){
        text = userName + " :\n" + completeText.left(urlPos);
    }
    else{
        text = completeText.left(urlPos);
    }


    //Media
    QPixmap pixmapLoaded;
    if(!pixmapLoaded.load(imagePath)){
        qDebug()<<"Image loading failed";
        return;      //Only continue if loading succeded
    }
    mediaPixmap.swap(pixmapLoaded);

    update();
}

void TwitDisplay::displaySplashScreen(){
    if(settingsFile->contains("Display/SplashScreen")){
        QString splashScreenPath = settingsFile->value("Display/SplashScreen").toString();
        QPixmap splashScreenPixmap;
        if(splashScreenPixmap.load(splashScreenPath)){
            mediaPixmap.swap(splashScreenPixmap);
            update();
        }
    }
}

void TwitDisplay::paintEvent(QPaintEvent *event){
    Q_UNUSED(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::SmoothPixmapTransform,true);

    //Background
    painter.fillRect(this->rect(),Qt::black);

    //Image
    qreal ratio = qMin((qreal)this->height() / (qreal)mediaPixmap.height(), //Find scaling factor...
                       (qreal)this->width()  / (qreal)mediaPixmap.width()); //... with same aspect ratio
    qreal drawingAreaWidth    = (qreal)mediaPixmap.width() * ratio;
    qreal drawingAreaHeight   = (qreal)mediaPixmap.height() * ratio;
    int horizontalMargins   = (this->width()-drawingAreaWidth) / 2.0;
    int verticalMargins     = (this->height()-drawingAreaHeight) / 2.0;
    painter.drawPixmap(horizontalMargins, verticalMargins,
                       drawingAreaWidth, drawingAreaHeight,
                       mediaPixmap);

    //Text
    if(textPixelSize>0){
        QFont font = painter.font();
        font.setPixelSize(textPixelSize);
        painter.setFont(font);

        painter.setPen(Qt::white);

        //Draw black area under text
        int textFlags = Qt::AlignHCenter | Qt::AlignBottom | Qt::TextWordWrap;
        QRect textRect = painter.fontMetrics().boundingRect(this->rect(),textFlags,text);
        textRect.setLeft(0);
        textRect.setRight(this->width());
        painter.fillRect(textRect,QColor(0,0,0,200));

        painter.drawText(this->rect(),textFlags,text);
    }

    //New icon on top left corner
    if(mediaIsNew)
        painter.drawPixmap(0,0,newMediaPixMap);

    //Status icon on top right corner
    painter.drawPixmap(this->width()-linkStatusPixMap.width(),0,linkStatusPixMap);
}

void TwitDisplay::keyPressEvent(QKeyEvent* event){
    if(isFullScreen()){
        event->accept();
        setFullScreen(false);
    }
}

void TwitDisplay::mouseDoubleClickEvent(QMouseEvent *event){
    Q_UNUSED(event);
    setFullScreen(!isFullScreen());
}

void TwitDisplay::setFullScreen(bool full){
    if(full)    this->showFullScreen();
    else        this->showNormal();
    toggleFullScreenAction->setChecked(this->isFullScreen());
}

void TwitDisplay::show(){
    if(settingsFile && settingsFile->value("Display/StartFullScreen",QVariant(false)).toBool())
        QWidget::showFullScreen();
    else
        QWidget::showNormal();
}

void TwitDisplay::setLinkStatus(LinkStatus status){
    switch(status){
    case LinkDown:
        linkStatusPixMap.load(QStringLiteral(":/connection/r"));
        break;

    case LinkWait:
        linkStatusPixMap.load(QStringLiteral(":/connection/y"));
        break;

    case LinkUp:
        linkStatusPixMap.load(QStringLiteral(":/connection/g"));
        break;
    }

    update();
}

void TwitDisplay::setSettingsFile(QSettings *settings){
    settingsFile=settings;

    if(settingsFile && settingsFile->contains("Display/TextSize"))
        textPixelSize = settingsFile->value("Display/TextSize").toInt();
}
