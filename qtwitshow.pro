#-------------------------------------------------
#
# Project created by QtCreator 2015-09-21T19:32:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(o2/o2.pri)

TARGET = qtwitshow
TEMPLATE = app


SOURCES += main.cpp\
    twitstreamer.cpp \
    twitdisplay.cpp \
    simplifiedkeystore.cpp \
    mediadownloader.cpp \
    showplanner.cpp

HEADERS  += \
    twitstreamer.h \
    twitdisplay.h \
    simplifiedkeystore.h \
    mediadownloader.h \
    showplanner.h

FORMS    +=

RESOURCES += \
    icons.qrc
