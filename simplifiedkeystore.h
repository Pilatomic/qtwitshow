#ifndef SIMPLIFIERKEYSTORE_H
#define SIMPLIFIERKEYSTORE_H

#include "o2abstractstore.h"

#include <QObject>
#include <QString>
#include <QMap>

class SimplifiedKeyStore: public O2AbstractStore
{
    Q_OBJECT
public :
    explicit SimplifiedKeyStore(QObject *parent = 0);
    QString value(const QString &key, const QString &defaultValue = QString());
    void setValue(const QString &key, const QString &value);

protected:
    QMap<QString,QString> keyMap;
};
#endif // SIMPLIFIERKEYSTORE_H
