#ifndef TWITSTREAMER_H
#define TWITSTREAMER_H

#include "o1twitter.h"
#include "o1requestor.h"
#include "simplecrypt.h"
#include "mediadownloader.h"

#include <QObject>
#include <QString>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QVariantMap>
#include <QRectF>
#include <QJsonDocument>
#include <QDir>
#include <QList>
#include <QSettings>
#include <QTimer>


struct TwitKeys{
    QString clientId;
    QString token;
    QString tokenSecret;
};

enum LinkStatus{LinkDown, LinkWait, LinkUp};


class TwitStreamer : public QObject
{
    Q_OBJECT
public:
    enum CriteriaType{CriteriaUndefined, CriteriaUser, CriteriaKeyword, CriteriaLocation};

    explicit TwitStreamer(QObject *parent = 0);

    void addTrackingCriteria(CriteriaType type, QString param);
    static CriteriaType getCriteriaType(O1RequestParameter criteria);

    TwitKeys getKeys();
    void setKeys(TwitKeys keys);

    bool setSavePath(QString path);

    void setAutoStartStreaming(bool enabled);

    void setSettingsFile(QSettings* newSettingsFile);

signals:
    void linkStatusUpdated( LinkStatus status);
    void newMediaAvailable(QString tweetPath, QString mediaPath);


public slots:
    void startAuthentication();
    void startStreaming();


private slots:
    void onLinkingSucceeded();
    void onOpenBrowser(const QUrl &url);
    void onCloseBrowser();
    void onDataReady();
    void onTimeout();
    void onConnectionFinished();

    void processTweet(QByteArray tweetData);

    void mediaDownloadComplete(QString tweetID, QString mediaPath);
    void mediaDownloadFailed();

public:
    QList<O1RequestParameter> criteriaList;
private :
    O1Twitter* o1Twitter_;
    O1Requestor* requestor;
    QNetworkAccessManager* nam;
    QNetworkReply* reply;
    QByteArray receiveBuffer;

    QList<MediaDownloader*> mediaDownloadQueue;
    void startNextMediaDownload();

    QDir saveDir;
    bool createFolderIfNotFound(QDir dir);

    SimpleCrypt crypt;
    quint64 getHash(const QString &encryptionKey);

    QSettings* settingsFile;

    bool autoStartStreaming;

    QTimer* timeoutDetector;
    QTimer* reconnectTimer;
};

#endif // TWITSTREAMER_H
