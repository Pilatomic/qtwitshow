#ifndef SHOWPLANNER_H
#define SHOWPLANNER_H

#include <QObject>
#include <QTimer>
#include <QMap>
#include <QQueue>
#include <QSettings>

class ShowPlanner : public QObject
{
    Q_OBJECT
public:
    explicit ShowPlanner(QObject *parent = 0);

signals:
    void mediaToDisplay(QString tweetPath, QString mediaPath, bool isNew);

public slots:
    void addMediaToShow(QString tweetPath, QString mediaPath);
    void loadSettings(QSettings* settingsFile);

private slots:
    void showNext();

private:
    QTimer* showTimer;
    QMap <QString,QString> mediaTweetMap;
    QQueue<QString> newMediaQueue;

    int defaultShowInterval;
    bool replayOldMedias;
    int intervalFactorThreshold;
    qreal intervalFactor;

    void updateTimerInterval(void);

};

#endif // SHOWPLANNER_H
